MyGame.path = (function(graphics) {

	let path = [
		{x: 0, y: 5},
		{x: 1, y: 5},
		{x: 2, y: 5},
		{x: 3, y: 5},
		{x: 3, y: 6},
		{x: 3, y: 7},
		{x: 3, y: 8},
		{x: 3, y: 9},
		{x: 3, y: 10},
		{x: 3, y: 11},
		{x: 3, y: 12},
		{x: 3, y: 13},
		{x: 4, y: 13},
		{x: 5, y: 13},
		{x: 6, y: 13},
		{x: 7, y: 13},
		{x: 7, y: 12},
		{x: 7, y: 11},
		{x: 7, y: 10},
		{x: 7, y: 9},
		{x: 7, y: 8},
		{x: 7, y: 7},
		{x: 7, y: 6},
		{x: 7, y: 5},
		{x: 7, y: 4},
		{x: 7, y: 3},
		{x: 7, y: 2},
		{x: 8, y: 2},
		{x: 9, y: 2},
		{x: 10, y: 2},
		{x: 11, y: 2},
		{x: 12, y: 2},
		{x: 13, y: 2},
		{x: 14, y: 2},
		{x: 15, y: 2},
		{x: 16, y: 2},
	]

	function render() {
		path.forEach(coord => {
			graphics.drawGrid(coord, '#b7b7b7', '#b7b7b7');
		})
	}
	
	function getPath() {
		return path;
	}

	return {
		render,
		getPath,
	}
})(MyGame.graphics);