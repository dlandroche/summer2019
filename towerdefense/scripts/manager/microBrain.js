MyGame.microBrain = (function () {
	const MUTATION_RATE = 0.2;
	const MAX_POOL = 20;
	const MIN_POOL = 10;

	let scores = [];
	let weightSum = 0;

	function sortScores() {
		scores = scores.sort((a, b) => {
			if (a.score > b.score) {
				return -1;
			} else if (b.score > a.score) {
				return 1;
			}
			return 0;
		})
		weightSum = 0;
		for(let i = 0; i < scores.length; i++) {
			let currWeight = scores.length -i;
			scores[i].weight = currWeight;
			weightSum += currWeight
		}
	}

	function removeOldDots() {
		scores = scores.sort((a, b) => {
			if (a.time > b.time) {
				return 1;
			} else if (b.time > a.time) {
				return -1;
			}
			return 0;
		})
		if (scores.length > MAX_POOL) {
			scores.shift();
		}
	}

	function reportDot(color, score, bonus) {
		scores.push({
			color: color,
			score: score,
			bonus: bonus,
			time: Date.now()
		});
		removeOldDots();
	}

	function getRandomDot(movementRate, baseHealth) {
		let bonus = Random.nextDouble();
		return {
			position: 0,
			shouldDelete: false,
			color: Utils.getRandColor(),
			timeToMove: movementRate,
			speed: 1.0 + bonus,
			health: (baseHealth / 100) + (1 - bonus),
		}
	}

	function getWeightedRandomDot() {
		let r = Random.nextRange(1,weightSum);
		for(let i =0; i < scores.length; i++) {
			r -= scores[i].weight;
			if(r <= 0) {
				return scores[i];
			}
		}
	}

	function getNextDot(movementRate, baseHealth) {
		if (scores.length < MIN_POOL) {
			return getRandomDot(movementRate, baseHealth);
		} else {
			sortScores();

			if(Random.nextDouble() < MUTATION_RATE) {
				return getRandomDot(movementRate, baseHealth);
			}else {
				let weightedRandom = getWeightedRandomDot();
				let dot = {
					position: 0,
					shouldDelete: false,
					color: weightedRandom.color,
					timeToMove: movementRate,
					bonus: weightedRandom.bonus,
					speed: 1.0 + weightedRandom.bonus,
					health: (baseHealth / 100) + (1 - weightedRandom.bonus)
				};
				return dot;
			}
		}
	}

	return {
		getNextDot,
		reportDot
	};
})();
