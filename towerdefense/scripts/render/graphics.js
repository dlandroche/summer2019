MyGame.graphics = (function () {
	'use strict';

	let canvas = document.getElementById('id-canvas');
	let context = canvas.getContext('2d');

	//------------------------------------------------------------------
	//
	// Public function that allows the client code to clear the canvas.
	//
	//------------------------------------------------------------------
	function clear() {
		context.clearRect(0, 0, canvas.width, canvas.height);
	}

	// --------------------------------------------------------------
	//
	// Draws a texture to the canvas with the following specification:
	//    image: Image
	//    center: {x: , y: }
	//    size: { width: , height: }
	//
	// --------------------------------------------------------------
	function drawTexture(image, center, rotation, size) {
		context.save();

		context.translate(center.x, center.y);
		context.rotate(rotation);
		context.translate(-center.x, -center.y);

		context.drawImage(
			image,
			center.x - size.x / 2,
			center.y - size.y / 2,
			size.x, size.y);

		context.restore();
	}

	function drawGridCircle(coord, fill, border) {
		let x = coord.x*100;
		let y = coord.y*100;
		let r = 30;

		drawCircle({
			center: {
				x: x-50,
				y: y-50,
			},
			radius: r
		}, fill, border);

	}

	function drawGrid(coord, fill, border) {
		let x = coord.x*100;
		let y = coord.y*100;
		
		drawRectangle({
			center: {
				x: x - 50,
				y: y-50,
			},
			size: {
				x: 100,
				y: 100,
			}
		}, fill, border)
	}

	// --------------------------------------------------------------
	//
	// Draw a rectangle to the canvas with the following attributes:
	//      center: { x: , y: },
	//      size: { x: , y: },
	//      rotation:       // radians
	//
	// --------------------------------------------------------------
	function drawRectangle(rect, fillStyle, strokeStyle) {
		context.save();
		context.translate(rect.center.x, rect.center.y);
		context.rotate(rect.rotation);
		context.translate(-rect.center.x, -rect.center.y);

		context.fillStyle = fillStyle;
		context.fillRect(rect.center.x - rect.size.x / 2, rect.center.y - rect.size.y / 2, rect.size.x, rect.size.y);

		context.strokeStyle = strokeStyle;
		context.strokeRect(rect.center.x - rect.size.x / 2, rect.center.y - rect.size.y / 2, rect.size.x, rect.size.y);

		context.restore();
	}

	function drawCircle(circle, fillStyle, strokeStyle, text) {
		context.save();

		context.beginPath();
		context.arc(circle.center.x, circle.center.y, circle.radius, 0, 2 * Math.PI, false);

		context.fillStyle = fillStyle;
		context.fill();

		if(text) {
			context.font = "bold 20pt Calibri";
			context.textAlign = "center";
			context.textBaseline = 'middle';
			context.fillStyle = "white";
			context.fillText(text, circle.center.x, circle.center.y);
		}

		if (strokeStyle) {
			context.lineWidth = 1;
			context.strokeStyle = strokeStyle;
			context.stroke();
		}

		context.restore();
	}

	return {
		clear,
		drawTexture,
		drawRectangle,
		drawGrid,
		drawCircle,
		drawGridCircle
	};
}());
