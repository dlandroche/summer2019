let Utils = (function () {

	function getRandColor() {
		let rand = Random.nextRange(0, 3);
		let color = 'red';
		if (rand == 0) {
			color = "green";
		} else if (rand == 1) {
			color = "blue";
		}
		return color;
	}

	function degreesToRadians(degrees) {
		return degrees * (Math.PI / 180);
	}

	function radiansToDegrees(radians) {
		let res = radians * (180 / Math.PI);
		while (res < 0) {
			res += 360;
		}
		while (res > 360) {
			res -= 360;
		}
		return res;
	}

	function isCollision(centerA, radiusA, centerB, radiusB) {
		let collisionDistance = radiusA / 2 + radiusB / 2;
		let normDist = getDist(centerA, centerB);
		return normDist < collisionDistance;
	}

	function getDist(centerA, centerB) {
		let xDist = centerA.x - centerB.x;
		let yDist = centerA.y - centerB.y;
		return Math.sqrt(xDist * xDist + yDist * yDist);
	}

	function getAverage(array) {
		let size = array.length;
		let sum = 0;
		for (let i = 0; i < size; i++) {
			sum += array[i];
		}
		return sum / size;
	}

	return {
		isCollision,
		getDist,
		radiansToDegrees,
		degreesToRadians,
		getAverage,
		getRandColor
	};
})();
