MyGame.main = (function (graphics, dots, obstacles) {
	let lastTimeStamp = performance.now();
	const FRAME_TIME = 1000 / 60; // 60 fps
	let currentFrameTime = 0;
	let once = false;
	let generationNumber = 1;

	let goal = {
		center: {
			x: 500,
			y: 50
		},
		radius: 25
	}

	function update(elapsedTime) {
		if (dots.areDotsLiving()) {
			dots.update(elapsedTime);
		} else if (!once) {
			once = true;
			dots.scoreDots();
		} else {
			once = false;
			generationNumber += 1;
			dots.nextGeneration();
		}
	}

	function render() {
		graphics.clear();
		graphics.drawCircle(goal, "#a7ff9b", 'black');
		obstacles.render();
		document.getElementById('generation').innerHTML = generationNumber;
		dots.render();
		document.getElementById('frame-time').innerHTML = currentFrameTime;
	}

	function gameLoop(time) {
		let elapsedTime = time - lastTimeStamp;
		currentFrameTime = Math.round(elapsedTime) + ' ms';
		update(elapsedTime);
		lastTimeStamp = time;
		render();
		requestAnimationFrame(gameLoop);
	}

	function start() {
		document.getElementById('start-button').style.display = "none";
		dots.createDots();
		requestAnimationFrame(gameLoop);
	}

	return {
		start
	}
})(
	MyGame.graphics,
	MyGame.dotManager,
	MyGame.obstacleManager
);