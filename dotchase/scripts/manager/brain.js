MyGame.brain = function (size) {

	let movements = [];
	let counter = 0;

	for (let i = 0; i < size; i++) {
		movements.push(getRandMovement());
	}

	function getRandMovement() {
		let direction = Random.randomDirection();
		let magnitude = Random.nextRange(7, 15);
		return { direction: direction, magnitude: magnitude }
	}

	function getNextMovement() {
		if (counter < movements.length) {
			let val = movements[counter];
			counter++;
			return val;
		} else {
			return "DEAD";
		}
	}

	function setMovements(m) {
		movements = [];
		m.forEach(mov => {
			movements.push(mov);
		});
		counter = 0;
	}

	function getMovements() {
		let val = [];
		movements.forEach(mov => {
			val.push(mov);
		});
		return val;
	}

	function mutate() {
		let mr = document.getElementById('mutation-rate').value;
		let mutationRate = parseInt(mr) / 100;
		for (let i = 0; i < movements.length; i++) {
			let r = Random.nextDouble();
			if (r < mutationRate) {
				movements[i] = getRandMovement();
			}
		}
	}

	function getCounter() {
		return counter;
	}

	return {
		getNextMovement,
		setMovements,
		getMovements,
		mutate,
		getCounter
	}

};