MyGame.towerManager = (function(graphics, bulletManager, path) {
	const ADD_BULLET_TIMEOUT = 300;

	let towers = [];

	function getShotgunTower(location, color) {
		return {
			location: location,
			color: color,
			bulletBurndownTime: ADD_BULLET_TIMEOUT,
			diagonal: false,
			addBullet: (location, color, diagonal) => {
				if(diagonal) {
					bulletManager.addBullet(location, Math.PI / 4, color);
					bulletManager.addBullet(location, (3 * Math.PI) / 4, color);
					bulletManager.addBullet(location, (5 * Math.PI) / 4, color);
					bulletManager.addBullet(location, (7 * Math.PI) / 4, color);
				} else {
					bulletManager.addBullet(location, 0, color);
					bulletManager.addBullet(location, (3 * Math.PI) / 2, color);
					bulletManager.addBullet(location, Math.PI, color);
					bulletManager.addBullet(location, Math.PI / 2, color);
				}
			}
		};
	}

	towers.push(
		getShotgunTower({ x: 6, y: 6 }, "blue"),
		getShotgunTower({ x: 4, y: 12 }, "green"),
		getShotgunTower({ x: 6, y: 12 }, "red"),
		getShotgunTower({ x: 2, y: 6 }, "blue"),
		getShotgunTower({ x: 8, y: 3 }, "green"),
		getShotgunTower({ x: 2, y: 9 }, "red")
	);

	function update(elapsedTime) {
		towers.forEach(tower => {
			tower.bulletBurndownTime -= elapsedTime;

			if (tower.bulletBurndownTime <= 0) {
				tower.bulletBurndownTime = ADD_BULLET_TIMEOUT;
				tower.addBullet(tower.location, tower.color, tower.diagonal);
				tower.diagonal = !tower.diagonal;
			}
		});
	}

	function render() {
		towers.forEach(tower => {
			graphics.drawGridCircle(
				{ x: tower.location.x, y: tower.location.y },
				tower.color,
				tower.color
			);
		});
	}

	function addTower(location, color) {
		if (!color) {
			color = "blue";
		}
		location = getGridLocation(location);
		let result = towers.filter(
			x => x.location.x == location.x && x.location.y == location.y
		);
		let isOnPath = path.getPath().filter(p=> p.x == location.x && p.y == location.y).length > 0;
		if (result.length == 0 &&!isOnPath) {
			towers.push(getShotgunTower(location, color));
			let towersCount = parseInt(document.getElementById("towersPlaced").innerHTML);
			towersCount = towersCount +1;
			document.getElementById("towersPlaced").innerHTML = towersCount;
		}
	}

	function getGridLocation(location) {
		return {
			x: Math.floor(location.x / 100) + 1,
			y: Math.floor(location.y / 100) + 1
		};
	}

	function removeTower(location) {
		location = getGridLocation(location);
		let result = towers.filter(
			x => x.location.x == location.x && x.location.y == location.y
		);
		if (result.length > 0) {
			let towersCount = parseInt(document.getElementById("towersPlaced").innerHTML);
			towersCount = towersCount -1;
			document.getElementById("towersPlaced").innerHTML = towersCount;
			towers = towers.filter(x => result.some(y => y != x));
		}
	}

	return {
		update,
		render,
		addTower,
		removeTower,
		path
	};
})(MyGame.graphics, MyGame.bulletManager, MyGame.path);
