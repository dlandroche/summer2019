function toggleButtonClass(button) {
	let classToSet;
	let previousButtonStatus;

	if (isButtonEnabled(button)) {
		previousButtonStatus = false;
		classToSet = "button";
	} else {
		previousButtonStatus = true;
		classToSet = "selected-button button";
	}

	button.setAttribute("class", classToSet);
	return previousButtonStatus;
}

function isButtonEnabled(button) {
	let currentClass = button.getAttribute("class");
	return currentClass != "button";
}

function addTowerPress() {
	let button = document.getElementById("addTowerButton");
	if (toggleButtonClass(button)) {
		enableAddTower();
	} else {
		disableAddTower();
	}
}

function enableAddTower() {
	let canvas = document.getElementById("id-canvas");
	canvas.addEventListener(
		"click",
		MyGame.clickHandlers.find(x => x.name == "addTowerHandler").function
	);
}

function disableAddTower() {
	let canvas = document.getElementById("id-canvas");
	canvas.removeEventListener(
		"click",
		MyGame.clickHandlers.find(x => x.name == "addTowerHandler").function
	);
}

function sellTowerPress() {
	let button = document.getElementById("sellTowerButton");
	if (toggleButtonClass(button)) {
		enableSellTower();
		disableAddTower();
	} else {
		disableSellTower();
	}
}

function enableSellTower() {
	let canvas = document.getElementById("id-canvas");
	canvas.addEventListener(
		"click",
		MyGame.clickHandlers.find(x => x.name == "sellTowerHandler").function
	);
}

function disableSellTower() {
	let canvas = document.getElementById("id-canvas");
	canvas.removeEventListener(
		"click",
		MyGame.clickHandlers.find(x => x.name == "sellTowerHandler").function
	);
}