

MyGame.clickHandlers = [];

MyGame.main = (function(
	graphics,
	enemyManager,
	path,
	towerManager,
	bulletManager
) {
	let lastTimeStamp = performance.now();

	function update(elapsedTime) {
		enemyManager.update(elapsedTime);
		towerManager.update(elapsedTime);
		bulletManager.update(elapsedTime);
	}

	function render() {
		graphics.clear();
		path.render();

		enemyManager.render();
		bulletManager.render();
		towerManager.render();
	}

	function gameLoop(time) {
		if (parseInt(document.getElementById("remainingLife").innerHTML) <= 0) {
			return;
		}
		let elapsedTime = time - lastTimeStamp;
		update(elapsedTime);
		lastTimeStamp = time;
		render();
		requestAnimationFrame(gameLoop);
	}

	document.getElementById("remainingLife").innerHTML = 100;
	document.getElementById("currentScore").innerHTML = 0;
	requestAnimationFrame(gameLoop);

	function getMousePos(canvas, event) {
		var rect = canvas.getBoundingClientRect();
		let context = canvas.getContext("2d");
		let xScale = context.canvas.width;
		let yScale = context.canvas.height;
		let percentage = {
			x: (event.clientX - rect.left) / rect.width,
			y: (event.clientY - rect.top) / rect.height
		};
		return {
			x: percentage.x * xScale,
			y: percentage.y * yScale
		};
	}

	let canvas = document.getElementById("id-canvas");

	function addTowerHandler(event) {
		if (
			parseInt(document.getElementById("towersPlaced").innerHTML) < 6
		) {
			var mousePos = getMousePos(canvas, event);
			var e = document.getElementById("selectedTowerType");
			var color = e.options[e.selectedIndex].value;
			towerManager.addTower(mousePos, color);
		}
	}

	function sellTowerHandler(event) {
		var mousePos = getMousePos(canvas, event);
		towerManager.removeTower(mousePos);
	}

	MyGame.clickHandlers.push({
		name: "addTowerHandler",
		function: addTowerHandler
	});

	MyGame.clickHandlers.push({
		name: "sellTowerHandler",
		function: sellTowerHandler
	});

	return {};
})(
	MyGame.graphics,
	MyGame.enemyManager,
	MyGame.path,
	MyGame.towerManager,
	MyGame.bulletManager
);
