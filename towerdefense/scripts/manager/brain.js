MyGame.brain = (function () {
	let counter = 0;
	const BATCH_SIZE = 20;
	const CHAOS = 0.1;

	let dots = [];
	let scores = [];

	function getRandomDot() {
		let bonus = Random.nextDouble();
		return {
			position: 0,
			shouldDelete: false,
			color: Utils.getRandColor(),
			bonus: bonus,
			speed: 1.0 + bonus,
			health: 0.6 + (1 - bonus),
		};
	}

	function getInitialBatch() {
		for (let i = 0; i < BATCH_SIZE; i++) {
			dots.push(getRandomDot())
		}
	}

	getInitialBatch();

	function sortScores() {
		scores = scores.sort((a, b) => {
			if (a.score > b.score) {
				return -1;
			} else if (b.score > a.score) {
				return 1;
			}
			return 0;
		})
	}

	function getNextBatch(baseHealth) {
		dots = [];
		sortScores();
		let average = Utils.getAverage(scores.map(x => x.score));
		scores.forEach(score => {
			if (score.score > average) {
				dots.push({
					position: 0,
					shouldDelete: false,
					color: score.color,
					speed: 1.0 + score.bonus,
					bonus: score.bonus,
					health: (baseHealth / 100) + (1 - score.bonus),
				})
			}
		})
		scores = [];
		while (dots.length < BATCH_SIZE) {
			let bonus = Random.nextDouble();
			dots.push({
				position: 0,
				shouldDelete: false,
				color: Utils.getRandColor(),
				bonus: bonus,
				speed: 1.0 + bonus,
				health: (baseHealth / 100) + (1 - bonus),
			})
		}
		runMutation();
	}

	function runMutation() {
		for (let i = 0; i < BATCH_SIZE; i++) {
			let rand = Random.nextDouble();
			if (rand < CHAOS) {
				dots[i].color = Utils.getRandColor();
			}
		}
	}

	function reportDot(color, score, bonus) {
		scores.push({ color: color, score: score, bonus: bonus });
	}

	function getNextDot(movementRate, baseHealth) {
		if (counter >= BATCH_SIZE) {
			getNextBatch(baseHealth);
			counter = 0;
		}

		let dot = dots[counter];
		counter++;
		dot.timeToMove = movementRate;
		return dot;
	}

	return {
		getNextDot,
		reportDot
	};
})();
