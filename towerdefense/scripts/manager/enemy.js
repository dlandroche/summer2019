MyGame.enemyManager = (function (graphics, brain, pathManager) {
	var MOVEMENT_RATE = 300;
	var ADD_RATE = 700;
	var BASE_HEALTH = 60;

	let addBurndown = 0;
	let path = pathManager.getPath();
	let enemies = [];

	setInterval(() => {
		if (MOVEMENT_RATE > 40) {
			MOVEMENT_RATE -= 5;
		}
		if (ADD_RATE > 100) {
			ADD_RATE -= 7;
		}
		BASE_HEALTH += 5;
		console.log(MOVEMENT_RATE, ADD_RATE);
	}, 5000);

	function createEnemy() {
		enemies.push(brain.getNextDot(MOVEMENT_RATE, BASE_HEALTH));
	}

	function checkCollision(bullet) {
		let isCollision = false;
		enemies.forEach(enemy => {
			if (
				Utils.isCollision(
					bullet.currentLocation,
					15,
					getCurrentPosition(enemy),
					40
				)
			) {
				if (bullet.color != enemy.color) {
					let bulletDamage = Math.random() * (1.0 - 0.4) + 0.4;
					brain.reportDot(enemy.color, enemy.position, enemy.speed - 1.0);
					enemy.health -= bulletDamage;
					isCollision = true;

					let currentScore = document.getElementById("currentScore")
						.innerHTML;
					document.getElementById("currentScore").innerHTML =
						parseInt(currentScore) + 5;
				}
			}
		});
		return isCollision;
	}

	function update(elapsedTime) {
		addBurndown -= elapsedTime;

		enemies.forEach(enemy => {
			enemy.timeToMove -= elapsedTime * enemy.speed;

			if (enemy.health < 0) {
				enemy.shouldDelete = true;
			}

			if (enemy.timeToMove <= 0 && enemy.position < path.length - 1) {
				enemy.position += 1;
				enemy.timeToMove = MOVEMENT_RATE;
			}

			if (enemy.position >= path.length - 1) {
				brain.reportDot(enemy.color, enemy.position, enemy.speed - 1.0);
				let currLife = (document.getElementById(
					"remainingLife"
				).innerHTML -= 5);
				document.getElementById("remainingLife").innerHTML = currLife;
				enemy.shouldDelete = true;
			}
		});

		enemies = enemies.filter(x => !x.shouldDelete);

		if (addBurndown <= 0) {
			createEnemy();
			addBurndown = ADD_RATE;
		}
	}

	function getCurrentPosition(enemy) {
		let direction;

		if (enemy.position < path.length - 1) {
			if (path[enemy.position].x == path[enemy.position + 1].x) {
				direction =
					path[enemy.position].y > path[enemy.position + 1].y
						? "up"
						: "down";
			} else {
				direction =
					path[enemy.position].x > path[enemy.position + 1].x
						? "left"
						: "right";
			}
		}

		let coord = path[enemy.position];
		let offset = 1 - enemy.timeToMove / MOVEMENT_RATE;

		let x = coord.x * 100;
		let y = coord.y * 100;

		switch (direction) {
			case "up":
				y -= offset * 100;
				break;
			case "down":
				y += offset * 100;
				break;
			case "left":
				x -= offset * 100;
				break;
			case "right":
				x += offset * 100;
				break;
		}

		return {
			x: x - 50,
			y: y - 50
		};
	}

	function render() {
		enemies.forEach(enemy => {
			let position = getCurrentPosition(enemy);
			let r = 30;
			let fill = enemy.color;

			graphics.drawCircle(
				{
					center: {
						x: position.x,
						y: position.y
					},
					radius: r
				},
				fill,
				fill,
				Math.floor(enemy.health * 100)
			);
		});
	}

	return {
		update,
		render,
		checkCollision
	};
})(MyGame.graphics, MyGame.brain, MyGame.path);
