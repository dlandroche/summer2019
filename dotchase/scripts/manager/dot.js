MyGame.dotManager = (function (
	graphics,
	brainCreator,
	obstacles
) {

	let GENERATION_SIZE = parseInt(document.getElementById('dot-count').value);
	let maxMovements = parseInt(document.getElementById('step-count').value);
	let currMovement = 0;

	let allDots = [];
	let goal = {
		center: {
			x: 500,
			y: 50
		},
		radius: 25
	}

	function createDots() {
		GENERATION_SIZE = parseInt(document.getElementById('dot-count').value);
		maxMovements = parseInt(document.getElementById('step-count').value);
		for (let i = 0; i < GENERATION_SIZE; i++) {
			allDots.push({
				center: {
					x: 500,
					y: 900
				},
				radius: 10,
				brain: new brainCreator(maxMovements),
				isLiving: true,
				color: '#d6a0ef',
			})
		}
	}
	function isTouchingGoal(dot) {
		return Utils.isCollision(dot.center, dot.radius, goal.center, goal.radius);
	}

	function isOutOfBounds(dot) {
		return (dot.center.x < 0 || dot.center.x > 1000 || dot.center.y < 0 || dot.center.y > 1000);
	}

	function update(elapsedTime) {
		currMovement++;
		if (currMovement > maxMovements) {
			allDots.forEach(x => x.isLiving = false);
		}
		allDots.forEach(dot => {
			if (dot.isLiving) {
				if (isTouchingGoal(dot) || isOutOfBounds(dot) || obstacles.isCollision(dot)) {
					dot.color = 'red'
					dot.isLiving = false;
				} else {
					let movement = dot.brain.getNextMovement()
					if (movement != "DEAD") {
						dot.center.x = dot.center.x + (movement.magnitude * Math.cos(movement.direction));
						dot.center.y = dot.center.y + (movement.magnitude * Math.sin(movement.direction));
					} else {
						dot.isLiving = false;
					}
				}
			}
		});
	}

	function render() {
		if (document.getElementById("render-all").checked) {
			allDots.forEach(dot => {
				graphics.drawCircle(dot, dot.color, "black");
			})
		} else {
			let yellowDot = allDots.filter(x => x.color == "#fff993");
			yellowDot = yellowDot.length > 0 ? yellowDot[0] : allDots[0];
			graphics.drawCircle(yellowDot, yellowDot.color, 'black');
		}
	}

	function areDotsLiving() {
		return allDots.some(x => x.isLiving)
	}

	function scoreDots() {
		allDots.forEach(dot => {
			if (Utils.isCollision(dot.center, dot.radius, goal.center, goal.radius)) {
				dot.score = 10000 - dot.brain.getCounter();
				dot.arrived = true;
			} else {
				let dist = Utils.getDist(dot.center, goal.center);
				let score = 1.0 / dist;
				dot.score = score;
				dot.arrived = false;
			}
		})
		let best = getBestDot()
		best.color = '#fff993';
	}

	function getBestDot() {
		allDots.sort((a, b) => {
			if (a.score < b.score) return 1;
			if (a.score > b.score) return -1;
			return 0;
		});
		return allDots[0];
	}

	function nextGeneration() {
		let parent = getBestDot();
		if (parent.arrived) {
			maxMovements = parent.brain.getCounter();
		}
		document.getElementById('best-score').innerHTML = maxMovements;
		currMovement = 0;
		let newGeneration = [];
		let newParent = new brainCreator(0);
		newParent.setMovements(parent.brain.getMovements());
		newGeneration.push({
			center: {
				x: 500,
				y: 900
			},
			radius: 10,
			brain: newParent,
			isLiving: true,
			color: '#fff993'
		});

		for (let i = 1; i < GENERATION_SIZE; i++) {
			let newBrain = new brainCreator(0);
			newBrain.setMovements(parent.brain.getMovements());
			newBrain.mutate();

			newGeneration.push({
				center: {
					x: 500,
					y: 900
				},
				radius: 10,
				brain: newBrain,
				isLiving: true,
				color: '#d6a0ef'
			})
		}
		allDots = newGeneration.reverse();
	}

	return {
		createDots,
		update,
		render,
		scoreDots,
		getBestDot,
		areDotsLiving,
		nextGeneration
	}

})(MyGame.graphics, MyGame.brain, MyGame.obstacleManager);