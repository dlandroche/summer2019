let Utils = (function () {

	function degreesToRadians(degrees) {
		return degrees * (Math.PI / 180);
	}

	function radiansToDegrees(radians) {
		let res = radians * (180 / Math.PI);
		while (res < 0) {
			res += 360;
		}
		while (res > 360) {
			res -= 360;
		}
		return res;
	}

	function isCollision(centerA, radiusA, centerB, radiusB) {
		let collisionDistance = radiusA / 2 + radiusB / 2;
		let normDist = getDist(centerA, centerB);
		return normDist < collisionDistance;
	}

	function getDist(centerA, centerB) {
		let xDist = centerA.x - centerB.x;
		let yDist = centerA.y - centerB.y;
		return Math.sqrt(xDist * xDist + yDist * yDist);
	}

	return {
		isCollision,
		getDist,
		radiansToDegrees,
		degreesToRadians
	};
})();
