MyGame.bulletManager = (function (graphics, enemyManager) {
	const BULLET_SPEED = 800 / 1000;
	const BULLET_LIFESPAN = 300;

	let bullets = [];
	let incrementer = 0;

	function addBullet(startLocation, direction, color) {
		bullets.push({
			currentLocation: {
				x: (startLocation.x * 100) - 50,
				y: (startLocation.y * 100) - 50
			},
			color: color,
			rotation: direction,
			remainingLife: BULLET_LIFESPAN,
			id: incrementer++
		})
	}

	function update(elapsedTime) {
		bullets.forEach(bullet => {
			let vectorX = Math.cos(bullet.rotation);
			let vectorY = Math.sin(bullet.rotation);

			bullet.currentLocation.x += vectorX * BULLET_SPEED * elapsedTime;
			bullet.currentLocation.y += vectorY * BULLET_SPEED * elapsedTime;

			bullet.remainingLife -= elapsedTime;

			if (enemyManager.checkCollision(bullet)) {
				bullets = bullets.filter(x => x != bullet);
				bullet.remainingLife = -10;
			}
		});

		bullets = bullets.filter(x => x.remainingLife > 0);
	}

	function render() {
		bullets.forEach(bullet => {
			graphics.drawCircle({
				center: {
					x: bullet.currentLocation.x,
					y: bullet.currentLocation.y
				},
				radius: 10,
			}, bullet.color, bullet.color)
		})
	}

	function removeBullet(id) {
		bullets = bullets.filter(x => x.id !== id);
	}

	return {
		addBullet,
		update,
		render,
		removeBullet
	}
})(MyGame.graphics, MyGame.enemyManager);