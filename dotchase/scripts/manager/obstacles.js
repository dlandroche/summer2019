MyGame.obstacleManager = (function (
	graphics,
) {
	const rect1 = {
		center: {
			x: 300,
			y: 300,
		},
		size: {
			x: 650,
			y: 25,
		}
	}

	const rect2 = {
		center: {
			x: 700,
			y: 600,
		},
		size: {
			x: 650,
			y: 25,
		}
	}

	const rects = [rect1, rect2];

	function render() {
		if (document.getElementById("use-obstacles").checked) {
			rects.forEach(rect => {
				graphics.drawRectangle(rect, '#fc6f64', 'black');
			})
		}
	}

	function isCollision(dot) {
		let collision = false;
		if (document.getElementById("use-obstacles").checked) {
			rects.forEach(rect => {
				let leftBound = rect.center.x - (rect.size.x / 2);
				let rightBound = rect.center.x + (rect.size.x / 2);
				let upperBound = rect.center.y - (rect.size.y / 2);
				let lowerBound = rect.center.y + (rect.size.y / 2);
				if (dot.center.x < rightBound
					&& dot.center.x > leftBound
					&& dot.center.y > upperBound
					&& dot.center.y < lowerBound) {
					collision = true;
				}
			});
		}
		return collision;
	}

	return {
		render,
		isCollision
	}

})(MyGame.graphics);